# Copyright © 2024 Dylan Aïssi <dylan.aissi@collabora.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

package Dpkg::Vendor::Apertis;

use strict;
use warnings;

our $VERSION = '0.01';

use parent qw(Dpkg::Vendor::Debian);

=encoding utf8

=head1 NAME

Dpkg::Vendor::Apertis - Apertis vendor class

=head1 DESCRIPTION

This vendor class customizes the behaviour of dpkg scripts for Apertis
specific behavior and policies.

=cut

sub run_hook {
    my ($self, $hook, @params) = @_;

    if ($hook eq 'update-buildflags') {
        my $flags = shift @params;

        # Run the Debian hook to add flags
        $self->SUPER::run_hook($hook, $flags);

        my $flag = '-Wformat-overflow=2 -Wformat-truncation=2';
        $flags->append('CFLAGS', $flag);
        $flags->append('CXXFLAGS', $flag);
        $flags->append('OBJCFLAGS', $flag);
        $flags->append('OBJCXXFLAGS', $flag);
    } else {
        return $self->SUPER::run_hook($hook, @params);
    }
}

=head1 CHANGES

=head2 Version 0.xx

This is a private module.

=cut

1;
